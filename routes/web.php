<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function(){

    //cart
    Route::resource('cart', 'App\Http\Controllers\CartController');
    Route::patch('kosongkan/{id}', 'App\Http\Controllers\CartController@kosongkan');
    // cart detail
    Route::resource('cartdetail', 'App\Http\Controllers\CartDetailController');
    // route transaksi
    Route::resource('transaksi', 'App\Http\Controllers\TransaksiController');
    // alamat pengiriman
    Route::resource('alamatpengiriman', 'App\Http\Controllers\AlamatPengirimanController');
    // checkout
    Route::get('checkout', 'App\Http\Controllers\CartController@checkout');
    // rajaongkir
    Route::get('ongkir', 'App\Http\Controllers\OngkirController@ongkir')->name('ongkir');
    Route::get('getcity/ajax/{id}', 'App\Http\Controllers\OngkirController@getCity')->name('getcity.ajax');
    Route::post('getongkir/ajax/{id}', 'App\Http\Controllers\OngkirController@getOngkir')->name('getongkir.ajax');

    Route::get('/', [App\Http\Controllers\Admin\ProdukController::class, 'index'])->name('/');
    Route::resource('produk', App\Http\Controllers\Admin\ProdukController::class);
    Route::resource('kategori', App\Http\Controllers\Admin\KategoriController::class);
    Route::get('isikategori', [App\Http\Controllers\Admin\KategoriController::class, 'isi']);
    Route::resource('rasa', App\Http\Controllers\Admin\RasaController::class);
    Route::get('isirasa', [App\Http\Controllers\Admin\RasaController::class, 'isi']);
    Route::resource('carabayar', App\Http\Controllers\Admin\BayarController::class);
    Route::resource('pengiriman', App\Http\Controllers\Admin\PengirimanController::class);
    Route::resource('cart-session', App\Http\Controllers\CartSessionController::class);

});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
