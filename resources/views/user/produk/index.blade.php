@extends('admin.layouts.app')
@section('title','Produk')
@section('produk','active')


@section('content')
    <div class="app-content content ecommerce-application">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Produk</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
{{--                                    <li class="breadcrumb-item"><a href="{{route('produk.index')}}">Produk</a>--}}
{{--                                    </li>--}}
                                    <li class="breadcrumb-item active">Index
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->
                            <div class="container">
                                <div class="row">
                                  <div class="col col-md-8">
                                    @if(count($errors) > 0)
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-warning">{{ $error }}</div>
                                    @endforeach
                                    @endif
                                    @if ($message = Session::get('error'))
                                        <div class="alert alert-warning">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success">
                                            <p>{{ $message }}</p>
                                        </div>
                                    @endif
                                    <div id="produk">
                                        @foreach($produks as $produk)
                                        <div class="card ecommerce-card">
                                            <div class="card-body">
                                                <div class="item-img">
                                                    <img src="{{ asset('image/foto/'.$produk->foto) }}" style="float:left; margin-right:50px; max-height: 200px; min-height:200px; max-width: 200px; min-width:200px; object-fit:cover;" class="image-fluid card-img-top " alt="...">
                                                </div>
                                                <div class="item-name">
                                                    <h3>{{$produk->nama}}</h3>
                                                    <p class="card-text">
                                                        {{$produk->deskripsi}}
                                                    </p>
                                                        @isset($produk->kategori['kategori'])
                                                            <p class="text-muted"><b>Kategori :</b>{{$produk->kategori['kategori']}}</p>
                                                        @endisset
                                                        @empty($produk->kategori['kategori'])
                                                            <p class="text-muted">-</p>
                                                        @endempty
                                                        @isset($produk->rasa['rasa'])
                                                            <p class="text-muted"><b>Rasa :</b>{{$produk->rasa['rasa']}}</p>
                                                        @endisset
                                                        @empty($produk->rasa['rasa'])
                                                            <p class="text-muted">-</p>
                                                        @endempty
                                                    <div  class="item-wrapper">
                                                    <div  class="item-cost">
                                                        <h4  class="item-price">Rp{{$produk->harga}}</h4>
                                                        <p class="card-text shipping">
                                                            <span class="badge badge-pill badge-light-success">Free Shipping</span>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div style="position: absolute;top: 0;right: 0;padding-top: 120px;padding-right:50px;">
                                                <input type="hidden" name="produk_id" value="{{ $produk->id }}">
                                                <input type="hidden" name="user_id" value="{{ @ Auth::user()->id }}">
                                                    <button class="btn btn-block btn-danger" type="submit">
                                                    <i data-feather="" class="fa fa-shopping-cart"></i> Wishlist
                                                    </button>
                                                    <button class="btn btn-block btn-primary btn-add-to-cart" data-id="{{$produk->id}}">
                                                        <i class="fa fa-shopping-cart"></i> Tambahkan Ke Keranjang
                                                        </button>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                  </div>
                                  <div class="col col-md-4">
                                      <div class="card">
                                          <div class="card-header">
                                                <h4 class="card-title">Kategori</h4>
                                          </div>
                                          <div class="card-body">
                                                <ul class="list-group">
                                                    @foreach($kategoris as $kategori)
                                                    <li class="list-group-item">
                                                        <a href="" data-id="$kategori->id">
                                                            {{$kategori->kategori}}</a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                            </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {

            $('#kategori').click(function () {
                var id = $(this).data('id');
                $.ajax({
                    url: "{{ url('produk-kategori') }}/"+id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (response) {
                        let rows = '';
                        $.each(response.data, function (idx, data) { 
                        idx++
                        rows += 
                                    '<div class="card ecommerce-card">' +
                                        '<div class="card-body">' +
                                            '<div class="item-img">' +
                                                '<img src="{{ asset("image/foto/".' + data.foto + ')}}" style="float:left; margin-right:50px; max-height: 200px; min-height:200px; max-width: 200px; min-width:200px; object-fit:cover;" class="image-fluid card-img-top " alt="...">' +
                                            '</div>' +
                                            '<div class="item-name">' +
                                                '<h3>' +data.nama +'</h3>' +
                                                '<p class="card-text">' +data.deskripsi +
                                                '</p>' +
                                                            '<p class="text-muted"><b>Kategori :</b>' +produk.kategori.kategori +'</p>'+
                                                            '<p class="text-muted"><b>Rasa :</b>'+produk.rasa.rasa+'</p>' +
                                                '<div  class="item-wrapper">'+
                                                '<div  class="item-cost">'+
                                                    '<h4  class="item-price">Rp' + data.harga + '</h4>'+
                                                    '<p class="card-text shipping">'+
                                                        '<span class="badge badge-pill badge-light-success">Free Shipping</span>'+
                                                    '</p>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div style="position: absolute;top: 0;right: 0;padding-top: 120px;padding-right:50px;">'+
                                            '<input type="hidden" name="produk_id" value="' + data.id +'">'+
                                            '<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">'+
                                                '<button class="btn btn-block btn-danger" type="submit">'+
                                                '<i data-feather="" class="fa fa-shopping-cart"></i> Wishlist'+
                                                '</button>'+
                                                '<button class="btn btn-block btn-primary btn-add-to-cart" data-id="'+produk.id+'">'+
                                                    '<i class="fa fa-shopping-cart"></i> Tambahkan Ke Keranjang'+
                                                    '</button>'+
                                            '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                                })
                                $('#produk').html('');
                                $('#produk').append(rows);
                                if (feather) {
                                    feather.replace({
                                        width: 14,
                                        height: 14
                                    });
                                }
                    }
                });
            });

            $('.btn-add-to-cart').click(function () {
                var id = $(this).data('id');
                $.ajax({
                    url: "{{route('cartdetail.store')}}",
                    type: "POST",
                    data: {
                        produk_id: id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (response) {
                        toastr.success(response.message, 'Berhasil!', {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            $('#cart-items').html('');
                            let rows = ''
                            $.each(response.data.detail, function (idx, d) { 
                                let disabled = "";
                                if(d.qty <= 1)
                                    disabled = "disabled";
                                    rows += '<div class="col-12 col-md-12 mb-3">'+
                                                '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                                    '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                                '</div>'+
                                                    '<div class="media-body">'+
                                                        '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                        '<div class="media-heading">'+
                                                        ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                        '</div>'+
                                                    ' <div class="btn-group" role="group">'+
                                                        '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                            '<input type="hidden" name="param" value="kurang">'+
                                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                            '</button>'+
                                                            '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                            '</button>'+
                                                            '<input type="hidden" name="param" value="tambah">'+
                                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                            '+</button>'+
                                                            '</div>'+
                                                            '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';
                            })
                            $('#cart-total').html('Rp'+(response.total));
                            $('#cart-count').html((response.count));
                            $('.cart-count-label').html((response.count));
                            $('#cart-items').html(rows);
                                
                            if (feather) {
                                feather.replace({
                                    width: 14,
                                    height: 14
                                });
                            }
                    },
                    error: function (data) {
                        toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '.btn-decrease', function () {
                var id = $(this).data('id');
                var qty = Number($(this).parent().find('#qty').text())-1; 
                $.ajax({
                    url: "{{url('cartdetail')}}/"+id,
                    type: "POST",
                    data: {
                        qty: qty,
                        _token: '{{csrf_token()}}',
                        _method: 'PUT'
                    },
                    success: function (response) {
                        toastr.success(response.message, 'Berhasil!', {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            $('#cart-items').html('');
                            let rows = ''
                            $.each(response.data.detail, function (idx, d) { 
                                let disabled = "";
                                if(d.qty <= 1)
                                disabled = "disabled";
                                    rows += '<div class="col-12 col-md-12 mb-3">'+
                                                '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                                    '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                                '</div>'+
                                                    '<div class="media-body">'+
                                                        '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                        '<div class="media-heading">'+
                                                        ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                        '</div>'+
                                                    ' <div class="btn-group" role="group">'+
                                                        '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                            '<input type="hidden" name="param" value="kurang">'+
                                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                            '</button>'+
                                                            '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                            '</button>'+
                                                            '<input type="hidden" name="param" value="tambah">'+
                                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                            '+</button>'+
                                                            '</div>'+
                                                            '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';
                            })
                            $('#cart-total').html('Rp'+(response.total));
                            $('#cart-count').html((response.count));
                            $('.cart-count-label').html((response.count));
                            $('#cart-items').html(rows);
                                
                            if (feather) {
                                feather.replace({
                                    width: 14,
                                    height: 14
                                });
                            }
                    },
                    error: function (data) {
                        toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '.btn-increase', function () {
                var id = $(this).data('id');
                var qty = Number($(this).parent().find('#qty').text())+1;
                // var qty = Number($(this).data('qty'))+1; 
                $.ajax({
                    url: "{{url('cartdetail')}}/"+id,
                    type: "POST",
                    data: {
                        qty: qty,
                        _token: '{{csrf_token()}}',
                        _method: 'PUT'
                    },
                    success: function (response) {
                        toastr.success(response.message, 'Berhasil!', {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            $('#cart-items').html('');
                            let rows = ''
                            $.each(response.data.detail, function (idx, d) { 
                                let disabled = "";
                                if(d.qty <= 1)
                                disabled = "disabled";
                                    rows += '<div class="col-12 col-md-12 mb-3">'+
                                                '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                                    '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                                '</div>'+
                                                    '<div class="media-body">'+
                                                        '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                        '<div class="media-heading">'+
                                                        ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                        '</div>'+
                                                    ' <div class="btn-group" role="group">'+
                                                        '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                            '<input type="hidden" name="param" value="kurang">'+
                                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                            '</button>'+
                                                            '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                            '</button>'+
                                                            '<input type="hidden" name="param" value="tambah">'+
                                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                            '+</button>'+
                                                            '</div>'+
                                                            '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';
                            })
                            $('#cart-total').html('Rp'+(response.total));
                            $('#cart-count').html((response.count));
                            $('.cart-count-label').html((response.count));
                            $('#cart-items').html(rows);
                                
                            if (feather) {
                                feather.replace({
                                    width: 14,
                                    height: 14
                                });
                            }
                    },
                    error: function (data) {
                        toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '#btn-del', function () {
                var id = $(this).data('id');
                $.ajax({
                    url: "{{url('cartdetail')}}/"+id,
                    type: "POST",
                    data: {
                        _token: '{{csrf_token()}}',
                        _method: 'DELETE'
                    },
                    success: function (response) {
                        toastr.success(response.message, 'Berhasil!', {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            $('#cart-items').html('');
                            let rows = ''
                            $.each(response.data.detail, function (idx, d) { 
                                let disabled = "";
                                if(d.qty <= 1)
                                    disabled = "disabled";
                                    rows += '<div class="col-12 col-md-12 mb-3">'+
                                                '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                                    '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                                '</div>'+
                                                    '<div class="media-body">'+
                                                        '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                        '<div class="media-heading">'+
                                                        ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                        '</div>'+
                                                    ' <div class="btn-group" role="group">'+
                                                        '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                            '<input type="hidden" name="param" value="kurang">'+
                                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                            '</button>'+
                                                            '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                            '</button>'+
                                                            '<input type="hidden" name="param" value="tambah">'+
                                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                            '+</button>'+
                                                            '</div>'+
                                                            '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';
                            })
                            $('#cart-total').html('Rp'+(response.total));
                            $('#cart-count').html((response.count));
                            $('.cart-count-label').html((response.count));
                            $('#cart-items').html(rows);
                                
                            if (feather) {
                                feather.replace({
                                    width: 14,
                                    height: 14
                                });
                            }
                    },
                    error: function (data) {
                        toastr.error('Produk gagal dihapus', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '#btn-kosongkan', function () {
                $.ajax({
                    url: "{{url('cartdetail')}}/"+id,
                    type: "POST",
                    data: {
                        _token: '{{csrf_token()}}',
                        _method: 'DELETE'
                    },
                    success: function (response) {
                        toastr.success(response.message, 'Berhasil!', {
                                closeButton: true,
                                tapToDismiss: false
                            });
                            $('#cart-items').html('');
                            let rows = ''
                            $.each(response.data.detail, function (idx, d) { 
                                let disabled = "";
                                if(d.qty <= 1)
                                    disabled = "disabled";
                                    rows += '<div class="col-12 col-md-12 mb-3">'+
                                                '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                                    '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                                '</div>'+
                                                    '<div class="media-body">'+
                                                        '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                        '<div class="media-heading">'+
                                                        ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                        '</div>'+
                                                    ' <div class="btn-group" role="group">'+
                                                        '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                            '<input type="hidden" name="param" value="kurang">'+
                                                            '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                            '</button>'+
                                                            '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                            '</button>'+
                                                            '<input type="hidden" name="param" value="tambah">'+
                                                            '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                            '+</button>'+
                                                            '</div>'+
                                                            '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>';
                            })
                            $('#cart-total').html('Rp'+(response.total));
                            $('#cart-count').html((response.count));
                            $('.cart-count-label').html((response.count));
                            $('#cart-items').html(rows);
                                
                            if (feather) {
                                feather.replace({
                                    width: 14,
                                    height: 14
                                });
                            }
                    },
                    error: function (data) {
                        toastr.error('Produk gagal dihapus', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            
        });
        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    </script>
@endpush