@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div style="text-align:center" class="card-header"><h1>{{ __('CRUD') }}<br>{{ __('Menu Nomimono Cafe') }}</h1></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="row">
                            <div class="col-md-4">
                                <a class="btn btn-primary btn-lg col-sm"  href="{{url('/produk') }}">PRODUK</a><br><br>
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-primary btn-lg col-sm" href="{{url('/kategori') }}">KATEGORI</a>
                            </div>
                            <div class="col-md-4">
                                <a class="btn btn-primary btn-lg col-sm"  href="{{url('/rasa') }}">RASA</a><br><br>
                            </div>
                        </div>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
