@extends('admin.layouts.app')
@section('title','Checkout')
@section('transaksi','active')

@section('content')
<div class="app-content content ecommerce-application">
  <div class="row">
    <div class="col col-8">
      @if (count($errors) > 0)
      @foreach($errors->all() as $error)
          <div class="alert alert-warning">{{ $error }}</div>
      @endforeach
      @endif
      @if ($message = Session::get('error'))
          <div class="alert alert-warning">
              <p>{{ $message }}</p>
          </div>
      @endif
      @if ($message = Session::get('success'))
          <div class="alert alert-success">
              <p>{{ $message }}</p>
          </div>
      @endif
      <div class="row mb-2">
        <div class="col col-12 mb-2">
          <div class="card">
            <div class="card-header">
              Item
            </div>
            <div class="card-body">
              <table class="table table-stripped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Produk</th>
                    <th>Harga</th>
                    <th>Diskon</th>
                    <th>Qty</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($itemcart->detail as $detail)
                  <tr>
                    <td>
                    {{ $no++ }}
                    </td>
                    <td>
                    {{ $detail->produk->nama }}
                    <br />
                    {{ $detail->produk->kode_produk }}
                    </td>
                    <td>
                    {{ $detail->harga }}
                    </td>
                    <td>
                    {{ $detail->diskon }}
                    </td>
                    <td>
                    {{ $detail->qty }}
                    </td>
                    <td>
                    {{ $detail->subtotal }}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col col-12">
          <div class="card">
            <div class="card-header">Alamat Pengiriman</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-stripped">
                  <thead>
                    <tr>
                      <th>Nama Penerima</th>
                      <th>Alamat</th>
                      <th>No tlp</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  @if($itemalamatpengiriman)
                    <tr>
                      <td>
                        {{ $itemalamatpengiriman->nama_penerima }}
                      </td>
                      <td>
                        {{ $itemalamatpengiriman->alamat }}<br />
                        {{ $itemalamatpengiriman->kelurahan}}, {{ $itemalamatpengiriman->kecamatan}}<br />
                        {{ $itemalamatpengiriman->city->city_name}}, {{ $itemalamatpengiriman->province->provinces}} - {{ $itemalamatpengiriman->kodepos}}
                      </td>
                      <td>
                        {{ $itemalamatpengiriman->no_tlp }}
                      </td>
                      <td>
                        <a href="{{ route('alamatpengiriman.index') }}" class="btn btn-success btn-sm">
                          Ubah Alamat
                        </a>                        
                      </td>
                    </tr>
                  @endif
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer">
              <a href="{{ route('alamatpengiriman.index') }}" class="btn btn-sm btn-primary">
                Tambah Alamat
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col col-4">
      <div class="card">
        <div class="card-header">
          Ringkasan
        </div>
        <div class="card-body">
          <table class="table">
            <tr>
              <td>No Invoice</td>
              <td class="text-right">
                {{ $itemcart->no_invoice }}
              </td>
            </tr>
            <tr>
              <td>Subtotal</td>
              <td class="text-right">
                Rp{{ $itemcart->subtotal }}
              </td>
            </tr>
            <tr>
              <td>Diskon</td>
              <td class="text-right">
                Rp{{ $itemcart->diskon}}
              </td>
            </tr>
            <tr>
              <td>Ongkir</td>
              <td id="ongkir" class="text-right">
                Rp{{ $itemcart->ongkir}}
              </td>
            </tr>
            <tr>
              <td>Total</td>
              <td id="total" class="text-right">
                Rp{{ $itemcart->total}}
              </td>
            </tr>
          </table>
        </div>
        <div class="card-footer">
          <select data-destination="{{@$itemalamatpengiriman->kota}}" name="courier" id="" class="form-control">
            @isset($itemcart->ekspedisi)
            <option value="{{ old('courier', @$itemcart->ekspedisi) }}">
              {{ strtoupper(old('courier', @$itemcart->ekspedisi))}}{{ $itemcart->where('ekspedisi', 'pos') ? ' Indonesia' : '' }}
            </option>
            @endisset
            <option value="" holder>Pilih Pengiriman</option>
            <option value="jne">JNE</option>
            <option value="tiki">TIKI</option>
            <option value="pos">POS Indonesia</option>
          </select>
          <br>
          <form action="{{ route('transaksi.store') }}" method="post">
            @csrf()
            <button type="submit" class="btn btn-danger btn-block">Buat Pesanan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
  <script>
    $('select[name="courier"]').on('change', function (){
      var origin = 22;
      var destination = $(this).data('destination');
      var weight = 200;
      var courier = $(this).val();
        $.ajax({
          url: 'getongkir/ajax/' + courier,
          type: "POST",
          data: {
            '_token': $('input[name="_token"]').val(),
            'origin': origin,
            'destination': destination,
            'weight': weight,
            'courier': courier,
          },
          success: function (response) {
          toastr.success(response.message, 'Berhasil!', {
                  closeButton: true,
                  tapToDismiss: false
              });
              $('#ongkir').html('Rp'+(response.ongkir));
              $('#total').html('Rp'+(response.total));
                  
              if (feather) {
                  feather.replace({
                      width: 14,
                      height: 14
                  });
              }
          },
          error: function (data) {
              toastr.error('Kurir gagal dipilih', 'Gagal!', {
                  closeButton: true,
                  tapToDismiss: false
              });
          }
            });
    })
  </script>
@endpush