@extends('admin.layouts.app')
@section('title','Produk')
@section('cart','active')


@section('content')
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-left mb-0">Produk</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
{{--                                    <li class="breadcrumb-item"><a href="{{route('produk.index')}}">Produk</a>--}}
{{--                                    </li>--}}
                                <li class="breadcrumb-item active">Index
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                <div class="form-group breadcrumb-right">
                    <div class="dropdown">
                        <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                        <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                    </div>
                </div>
            </div>
        </div>
<div class="container">
    <div class="row">
      <div class="col col-md-8">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            <div class="alert alert-warning">{{ $error }}</div>
        @endforeach
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-warning">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <div class="card">
          <div class="card-header">
            Item
          </div>
          <div class="card-body">
            <table class="table table-stripped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Produk</th>
                  <th>Harga</th>
                  <th>Qty</th>
                  <th>Subtotal</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @isset($itemcart->detail)
                  @foreach($itemcart->detail as $detail)
                    <tr>
                      <td>
                      {{ $no++ }}
                      </td>
                      <td>
                      {{ $detail->produk->nama }}
                      <br />
                      {{ $detail->produk->kode_produk }}
                      </td>
                      <td>
                      {{ number_format($detail->produk->harga) }}
                      </td>
                      <td>
                        <div class="btn-group" role="group">
                          <form action="{{ route('cartdetail.update',$detail->id) }}" method="post">
                          @method('patch')
                          @csrf()
                            <input type="hidden" name="param" value="kurang">
                            <button class="btn btn-primary btn-sm">
                            -
                            </button>
                          </form>
                          <button class="btn btn-outline-primary btn-sm" disabled="true">
                          {{ number_format($detail->qty) }}
                          </button>
                          <form action="{{ route('cartdetail.update',$detail->id) }}" method="post">
                          @method('patch')
                          @csrf()
                            <input type="hidden" name="param" value="tambah">
                            <button class="btn btn-primary btn-sm">
                            +
                            </button>
                          </form>
                        </div>
                      </td>
                      <td>
                      {{ number_format($detail->subtotal) }}
                      </td>
                      <td>
                      <form action="{{ route('cartdetail.destroy', $detail->id) }}" method="post" style="display:inline;">
                        @csrf
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-sm btn-danger mb-2">
                          Hapus
                        </button>                    
                      </form>
                      </td>
                    </tr>
                  @endforeach
                @endisset
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col col-md-4">
        <div class="card">
          <div class="card-header">
            Ringkasan
          </div>
          <div class="card-body">
            <table class="table">
              <tr>
                <td>No Invoice</td>
                <td class="text-right">
                  {{ @$itemcart->no_invoice }}
                </td>
              </tr>
              <tr>
                <td>Subtotal</td>
                <td class="text-right">
                  {{ number_format(@$itemcart->subtotal) }}
                </td>
              </tr>
              <tr>
                <td>Diskon</td>
                <td class="text-right">
                  {{ number_format(@$itemcart->diskon) }}
                </td>
              </tr>
              <tr>
                <td>Total</td>
                <td class="text-right">
                  {{ number_format(@$itemcart->total) }}
                </td>
              </tr>
            </table>
          </div>
          <div class="card-footer">
            <div class="row">
              <div class="col">
                <a href="{{ url('checkout') }}" class="btn btn-primary btn-block">
                  Checkout
                </a>
              </div>
              <div class="col">
                <form action="{{ url('kosongkan').'/'.@$itemcart->id }}" method="post">
                  @method('patch')
                  @csrf()
                  <button type="submit" class="btn btn-danger btn-block">Kosongkan</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
  @endsection