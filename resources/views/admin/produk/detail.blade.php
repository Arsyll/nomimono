@extends('admin.layouts.app')
@section('title')
     Form {{'Detail Produk'}}
@endsection
@section('produk','active')
@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Data Produk</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('produk')}}">Index</a>
                                    </li>
                                    <li class="breadcrumb-item active">Detail Produk
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->
                <section id="basic-vertical-layouts">
                    <div class="row">
                        <div class=" col-12">
                            <div class="card">
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading">Error!</h4>
                                            <div class="alert-body">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="card-body">
                                        <div class="row my-2">
                                            <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                                <div class="d-flex align-items-center justify-content-center">
                                                    <img src="{{ asset('image/foto/'.$produk->foto) }}" class="img-fluid product-img" alt="product image" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-12 col-md-7">
                                                <h4>{{$produk->nama}}</h4>
                                                <div class="ecommerce-details-price d-flex flex-wrap mt-1">
                                                    <h4 class="item-price mr-1">Rp{{$produk->harga}}</h4>
                                                    <ul class="unstyled-list list-inline pl-1 border-left">
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="filled-star"></i></li>
                                                        <li class="ratings-list-item"><i data-feather="star" class="unfilled-star"></i></li>
                                                    </ul>
                                                </div>
                                                <p class="card-text">Stok = <span class="text-success">{{$produk->stok}}</span></p>
                                                <p class="card-text">
                                                    {{$produk->deskripsi}}
                                                </p>
                                                <ul class="product-features list-unstyled">
                                                    <li><i data-feather="shopping-cart"></i> <span>Gratis Ongkir</span></li>
                                                    <li>
                                                        <i data-feather="dollar-sign"></i>
                                                        <span>Tersedia COD</span>
                                                    </li>
                                                </ul>
                                                <hr />
                                                <div >
                                                    <h6>Kategori</h6>
                                                    @isset($produk->kategori['kategori'])
                                                        <p class="text-muted">{{$produk->kategori['kategori']}}</p>
                                                    @endisset
                                                    @empty($produk->kategori['kategori'])
                                                        <p class="text-muted">-</p>
                                                    @endempty
                                                    
                                                    <h6>Rasa</h6>
                                                    @isset($produk->rasa['rasa'])
                                                        <p class="text-muted">{{$produk->rasa['rasa']}}</p>
                                                    @endisset
                                                    @empty($produk->rasa['rasa'])
                                                        <p class="text-muted">-</p>
                                                    @endempty
                                                    
                                                    
                                                    
                                                    
                                                </div>
                                                <hr />
                                                @can('isUser')
                                                <div class="d-flex flex-column flex-sm-row pt-1">
                                                    <a href="javascript:void(0)" class="btn btn-primary btn-cart mr-0 mr-sm-1 mb-1 mb-sm-0">
                                                        <i data-feather="shopping-cart" class="mr-50"></i>
                                                        <span class="add-to-cart">Add to cart</span>
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-outline-secondary btn-wishlist mr-0 mr-sm-1 mb-1 mb-sm-0">
                                                        <i data-feather="heart" class="mr-50"></i>
                                                        <span>Wishlist</span>
                                                    </a>
                                                    <div class="btn-group dropdown-icon-wrapper btn-share">
                                                        <button type="button" class="btn btn-icon hide-arrow btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i data-feather="share-2"></i>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a href="javascript:void(0)" class="dropdown-item">
                                                                <i data-feather="facebook">Facebook</i>
                                                            </a>
                                                            <a href="javascript:void(0)" class="dropdown-item">
                                                                <i data-feather="twitter">Twitter</i>
                                                            </a>
                                                            <a href="javascript:void(0)" class="dropdown-item">
                                                                <i data-feather="youtube">YouTube</i>
                                                            </a>
                                                            <a href="javascript:void(0)" class="dropdown-item">
                                                                <i data-feather="instagram">Instragram</i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elsecan('isAdmin')
                                                <div class="d-flex flex-column flex-sm-row pt-1">
                                                    <a href="{{route('produk.edit', $produk->id)}}" class="btn btn-warning waves-effect waves-float waves-light mr-0 mr-sm-1 mb-1 mb-sm-0">
                                                        <span class="add-to-cart"> Edit</span>
                                                    </a>
                                                    <a href="#" data-id="{{$produk->id}}" class="btn btn-danger btn-del waves-effect waves-float waves-light mr-0 mr-sm-1 mb-1 mb-sm-0">
                                                        <span>Delete</span>
                                                    </a>
                                                </div>
                                                @endcan
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
    <script>

    </script>
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.btn-del', function () {
                var id = $(this).data('id');
                Swal.fire({
                    icon: 'error',
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                    .then((result) => {
                        if (result.value) {
                            $.ajax({
                                'url': '{{url('produk')}}/' + id,
                                'type': 'post',
                                'data': {
                                    '_method': 'DELETE',
                                    '_token': '{{csrf_token()}}'
                                },
                                success: function (response) {
                                    if (response == 1) {
                                        toastr.error('Data gagal dihapus!', 'Gagal!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                    } else {
                                        toastr.success('Data berhasil dihapus!', 'Berhasil!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                        window.location = "http://localhost:8181/";
                                    }
                                }
                            });
                        } else {
                            console.log(`dialog was dismissed by ${result.dismiss}`)
                        }

                    });
            });

        });

    </script>
@endpush

