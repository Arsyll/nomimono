@extends('admin.layouts.app')
@section('title','Produk')
@section('produk','active')


@section('content')
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Produk</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
{{--                                    <li class="breadcrumb-item"><a href="{{route('produk.index')}}">Produk</a>--}}
{{--                                    </li>--}}
                                    <li class="breadcrumb-item active">Index
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->@can('isAdmin')
                            <div class="header mb-2">
                                <a href="{{route('produk.create')}}" class="btn btn-primary waves-effect waves-float waves-light">Tambah Produk</a>
                            </div>
                            @endcan
                                <section class="card-content-types">
                                        <div class="row row-cols-4">
                                            @foreach ($produks as $produk)
                                            <div class="col-12 col-md-3 mb-3">
                                                <div class="card h-100" >
                                                    <div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">
                                                        <a href="{{url('/produk', $produk->id)}}">
                                                        <img src="{{ asset('image/foto/'.$produk->foto) }}" style="max-height: 200px; min-height:200px; object-fit:cover;" class="image-fluid card-img-top " alt="...">
                                                    </a>
                                                    </div>

                                                    <div class="card-body">
                                                        <ul class="list-unstyled d-flex justify-content-between">
                                                            <li>
                                                                <i>⭐</i>
                                                                <i>⭐</i>
                                                                <i>⭐</i>
                                                                <i>⭐</i>
                                                                <i>⭐</i>
                                                            </li>
                                                            <li class="text-muted text-right">Rp{{number_format($produk->harga)}}</li>
                                                        </ul>
                                                        <a href="{{url('/produk', $produk->id)}}" class="h3 text-decoration-none text-dark">{{$produk->nama}}</a>
                                                        <p class="card-text">
                                                            {{$produk->deskripsi}}
                                                        </p>
                                                        @if($produk->id_kategori>0)
                                                            <p class="text-muted">{{@$produk->kategori['kategori']}}</p>
                                                        @elseif($produk->id_kategori==0)
                                                            <p class="text-muted">-</p>
                                                        @endif
                                                        
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                        </div>
                                    </div>
                                </section>


                <!-- Basic Tables end -->
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.btn-add-to-cart').click(function () {
                var id = $(this).data('id');
                $.ajax({
                    url: "{{route('cart-session.store')}}",
                    type: "POST",
                    data: {
                        id_produk: id,
                        _token: '{{csrf_token()}}'
                    },
                    success: function (data) {
                        $('#cart-count').html(data.data.cart_item_count);
                        $('#cart-count-label').html(data.data.cart_item_count+" Items");
                        $('#cart-total').html(data.data.cart_total);
                        $('#cart-items').html(data.data.cart_items);
                        toastr.success('Keranjang berhasil diperbarui', 'Berhasil!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        if (feather) {
                            feather.replace({
                                width: 14,
                                height: 14
                            });
                        }
                    },
                    error: function (data) {
                        toastr.error('Keranjang gagal diperbarui', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '.btn-decrease', function () {
                var id = $(this).data('id');
                var qty = parseInt($(this).data('qty'))-1;
                $.ajax({
                    url: "{{url('cart-session')}}/"+id,
                    type: "POST",
                    data: {
                        qty: qty,
                        _token: '{{csrf_token()}}',
                        _method: 'PUT'
                    },
                    success: function (data) {
                        $('#cart-count').html(data.data.cart_item_count);
                        $('#cart-count-label').html(data.data.cart_item_count+" Items");
                        $('#cart-total').html(data.data.cart_total);
                        $('#cart-items').html(data.data.cart_items);
                        toastr.success('Keranjang berhasil diperbarui', 'Berhasil!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        if (feather) {
                            feather.replace({
                                width: 14,
                                height: 14
                            });
                        }
                    },
                    error: function (data) {
                        toastr.error('Keranjang gagal diperbarui', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '.btn-increase', function () {
                var id = $(this).data('id');
                var qty = parseInt($(this).data('qty'))+1;
                $.ajax({
                    url: "{{url('cart-session')}}/"+id,
                    type: "POST",
                    data: {
                        qty: qty,
                        _token: '{{csrf_token()}}',
                        _method: 'PUT'
                    },
                    success: function (data) {
                        $('#cart-count').html(data.data.cart_item_count);
                        $('#cart-count-label').html(data.data.cart_item_count+" Items");
                        $('#cart-total').html(data.data.cart_total);
                        $('#cart-items').html(data.data.cart_items);
                        toastr.success('Keranjang berhasil diperbarui', 'Berhasil!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                        if (feather) {
                            feather.replace({
                                width: 14,
                                height: 14
                            });
                        }
                    },
                    error: function (data) {
                        toastr.error('Keranjang gagal diperbarui', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $('#cart-items').on('click', '.cart-item-remove', function () {
                var id = $(this).data('id');
                $.ajax({
                    url: "{{url('cart-session')}}/"+id,
                    type: "POST",
                    data: {
                        _token: '{{csrf_token()}}',
                        _method: 'DELETE'
                    },
                    success: function (data) {
                        $('#cart-count').html(data.data.cart_item_count);
                        $('#cart-count-label').html(data.data.cart_item_count+" Items");
                        $('#cart-total').html(data.data.cart_total);
                        $('#cart-items').html(data.data.cart_items);
                        if (feather) {
                            feather.replace({
                                width: 14,
                                height: 14
                            });
                        }
                        toastr.success('Data berhasil dihapus!', 'Berhasil!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    },
                    error: function (data) {
                        toastr.error('Terjadi kesalahan!', 'Gagal!', {
                            closeButton: true,
                            tapToDismiss: false
                        });
                    }
                });
            });

            $(document).on('click', '.btn-del', function () {
                var id = $(this).data('id');
                Swal.fire({
                    icon: 'error',
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'error',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                })
                    .then((result) => {
                        if (result.value) {
                            $.ajax({
                                'url': '{{url('produk')}}/' + id,
                                'type': 'post',
                                'data': {
                                    '_method': 'DELETE',
                                    '_token': '{{csrf_token()}}'
                                },
                                success: function (response) {
                                    if (response == 1) {
                                        toastr.error('Data gagal dihapus!', 'Gagal!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                    } else {
                                        toastr.success('Data berhasil dihapus!', 'Berhasil!', {
                                            closeButton: true,
                                            tapToDismiss: false
                                        });
                                        location.reload();
                                    }
                                }
                            });
                        } else {
                            console.log(`dialog was dismissed by ${result.dismiss}`)
                        }

                    });
            });

        });

    </script>
@endpush

