@extends('admin.layouts.app')
@section('title')
     Form {{@$produk ? ' Ubah' : ' Tambah'}}
@endsection
@section('produk','active')
@section('content')

    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">Data Produk</h2>
                            <div class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('produk')}}">Index</a>
                                    </li>
                                @isset($produk->id)
                                    <li class="breadcrumb-item"> <a href="{{route('produk.show',$produk->id)}}">Detail Produk</a>
                                    </li>
                                @endisset
                                    
                                    <li class="breadcrumb-item active">{{(@$produk ? ' Ubah' : ' Tambah')}} Produk
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrumb-right">
                        <div class="dropdown">
                            <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i data-feather="grid"></i></button>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="app-todo.html"><i class="mr-1" data-feather="check-square"></i><span class="align-middle">Todo</span></a><a class="dropdown-item" href="app-chat.html"><i class="mr-1" data-feather="message-square"></i><span class="align-middle">Chat</span></a><a class="dropdown-item" href="app-email.html"><i class="mr-1" data-feather="mail"></i><span class="align-middle">Email</span></a><a class="dropdown-item" href="app-calendar.html"><i class="mr-1" data-feather="calendar"></i><span class="align-middle">Calendar</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- Basic Tables start -->@can('isAdmin')
                <section id="basic-vertical-layouts">
                    <div class="row">
                        <div class=" col-12">
                            <div class="card">
                                <div class="card-body">
                                    @if ($errors->any())
                                        <div class="alert alert-danger" role="alert">
                                            <h4 class="alert-heading">Error!</h4>
                                            <div class="alert-body">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    
                                    <form class="form form-vertical"  action="{{@$produk ? route('produk.update',$produk->id) : route('produk.store')}}"
                                          method="POST" enctype="multipart/form-data">
                                        @csrf
                                        @if(@$produk)
                                            {{method_field('patch')}}
                                        @endif
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <form enctype="multipart/form-data" action="{{@$produk ? url('produk',@$produk->id) : url('produk')}}" method="POST">
                                                        @csrf
                                                    
                                                        @if(!empty(@$produk))
                                                            @method('PATCH')
                                                        @endif
                                                    
                                                        <h5>Nama Produk :</h5> <input type="text" class="form-control" name="nama" value="{{ old('nama', @$produk->nama) }}"><br>
                                                        <h5>Deskripsi Produk :</h5> <textarea type="text" class="form-control" name="deskripsi" value="{{old('deskripsi', @$produk->deskripsi)}}">{{old('deskripsi', @$produk->deskripsi)}}</textarea> <br>
                                                        {{Form::label('id_kategori', 'Kategori',['class'=>'h5'])}}
                                                        {{Form::select('id_kategori', $kategorilist, null, ['class' =>'form-control'])}} 
                                                        <br>
                                                        {{Form::label('id_rasa', 'Rasa',['class'=>'h5'])}}
                                                        {{Form::select('id_rasa', $rasalist, null, ['class' =>'form-control'])}} 
                                                        <br>
                                                        <h5>Stok :</h5> <input type="number" min="0" class="form-control" name="stok" value="{{ old('stok', @$produk->stok) }}"><br>
                                                        <h5>Hpp  :</h5> <input type="number" min="0" class="form-control" name="hpp" value="{{ old('hpp', @$produk->hpp) }}"><br>
                                                        <h5>Harga  :</h5> <input type="number" min="0" class="form-control" name="harga" value="{{ old('harga', @$produk->harga) }}"><br>
                                                        <h5>Foto  :</h5> <input type="file" class="form-control" name="foto" value="{{ old('foto', @$produk->foto) }}"><br>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mr-1">Submit</button>
                                                @isset($produk->id)
                                                <a href="{{route('produk.show', $produk->id)}}" type="reset" class="btn btn-outline-secondary">Cancel</a>
                                                @endisset
                                                @empty($produk->id)
                                                <a href="{{url('produk')}}" type="reset" class="btn btn-outline-secondary">Cancel</a>
                                                @endempty
                                            </div>
                                            </form>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @elsecan('isUser')
                <meta http-equiv="refresh" content="0; url=http://127.0.0.1:8181/produk">
                @endcan
                <!-- Basic Tables end -->
            </div>
        </div>
    </div>
    @endsection

@push('styles')
@endpush

@push('scripts')
    <script>

    </script>
@endpush