<form class="form form-vertical"  action="{{route('rasa.update',$rasa->id)}}"
    method="POST" enctype="multipart/form-data">
  @csrf
  <div class="row">
      <div class="col-12">
          <div class="form-group">
              <label for="rasa">Rasa</label>
              <input type="text" id="rasa" class="form-control" name="rasa" placeholder="Rasa"
              value="{{$rasa->rasa}}"/>
          </div>
      </div>

    </div>
</form>