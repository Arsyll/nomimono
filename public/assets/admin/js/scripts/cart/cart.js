$(document).ready(function () {
    $('.btn-add-to-cart').click(function () {
        var id = $(this).data('id');
        $.ajax({
            url: "{{route('cartdetail.store')}}",
            type: "POST",
            data: {
                produk_id: id,
                _token: '{{csrf_token()}}'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.detail, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="col-12 col-md-12 mb-3">'+
                                        '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                            '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                        '</div>'+
                                            '<div class="media-body">'+
                                                '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                '<div class="media-heading">'+
                                                ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                '</div>'+
                                            ' <div class="btn-group" role="group">'+
                                                '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                    '<input type="hidden" name="param" value="kurang">'+
                                                    '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                    '</button>'+
                                                    '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                    '</button>'+
                                                    '<input type="hidden" name="param" value="tambah">'+
                                                    '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                    '+</button>'+
                                                    '</div>'+
                                                    '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                    })
                    $('#cart-total').html('Rp'+(response.total));
                    $('#cart-count').html((response.count));
                    $('#cart-count-label').html((response.count));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });

    $('#cart-items').on('click', '.btn-decrease', function () {
        var id = $(this).data('id');
        var qty = Number($(this).parent().find('#qty').text())-1; 
        $.ajax({
            url: "{{url('cartdetail')}}/"+id,
            type: "POST",
            data: {
                qty: qty,
                _token: '{{csrf_token()}}',
                _method: 'PUT'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.detail, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="col-12 col-md-12 mb-3">'+
                                        '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                            '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                        '</div>'+
                                            '<div class="media-body">'+
                                                '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                '<div class="media-heading">'+
                                                ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                '</div>'+
                                            ' <div class="btn-group" role="group">'+
                                                '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                    '<input type="hidden" name="param" value="kurang">'+
                                                    '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                    '</button>'+
                                                    '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                    '</button>'+
                                                    '<input type="hidden" name="param" value="tambah">'+
                                                    '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                    '+</button>'+
                                                    '</div>'+
                                                    '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                    })
                    $('#cart-total').html('Rp'+(response.total)); console.log(response);
                    $('#cart-count').html((response.count));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });

    $('#cart-items').on('click', '.btn-increase', function () {
        var id = $(this).data('id');
        var qty = Number($(this).parent().find('#qty').text())+1;
        // var qty = Number($(this).data('qty'))+1; 
        $.ajax({
            url: "{{url('cartdetail')}}/"+id,
            type: "POST",
            data: {
                qty: qty,
                _token: '{{csrf_token()}}',
                _method: 'PUT'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.detail, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="col-12 col-md-12 mb-3">'+
                                        '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                            '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                        '</div>'+
                                            '<div class="media-body">'+
                                                '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                '<div class="media-heading">'+
                                                ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                '</div>'+
                                            ' <div class="btn-group" role="group">'+
                                                '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                    '<input type="hidden" name="param" value="kurang">'+
                                                    '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                    '</button>'+
                                                    '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                    '</button>'+
                                                    '<input type="hidden" name="param" value="tambah">'+
                                                    '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                    '+</button>'+
                                                    '</div>'+
                                                    '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                    })
                    $('#cart-total').html('Rp'+(response.total)); console.log(response);
                    $('#cart-count').html((response.count));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Produk gagal ditambahkan', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });

    $('#cart-items').on('click', '#btn-del', function () {
        var id = $(this).data('id');
        $.ajax({
            url: "{{url('cartdetail')}}/"+id,
            type: "POST",
            data: {
                _token: '{{csrf_token()}}',
                _method: 'DELETE'
            },
            success: function (response) {
                toastr.success(response.message, 'Berhasil!', {
                        closeButton: true,
                        tapToDismiss: false
                    });
                    $('#cart-items').html('');
                    let rows = ''
                    $.each(response.data.detail, function (idx, d) { 
                        let disabled = "";
                        if(d.qty <= 1)
                            disabled = "disabled";
                            rows += '<div class="col-12 col-md-12 mb-3">'+
                                        '<div class="media align-items-center"><div class="panel-heading" style="text-align: center; overflow: hidden; padding: 0;">'+
                                            '<img class="d-block rounded mr-1" src="{{asset("image/foto")}}/'+d.produk.foto +'" style="max-height: 60px;min-height:60px; max-width: 60px; min-width:60px; object-fit:cover;" class="image-fluid card-img-top "  alt="..." >'+
                                        '</div>'+
                                            '<div class="media-body">'+
                                                '<i class="ficon cart-item-remove" id="btn-del" data-feather="x" data-id="'+d.id+'"></i>'+
                                                '<div class="media-heading">'+
                                                ' <h6 class="cart-item-title"><a class="text-body" href="app-ecommerce-details.html">'+d.produk.nama+'</a>'+
                                                '</div>'+
                                            ' <div class="btn-group" role="group">'+
                                                '<input type="hidden" class="product_id" value="'+d.id+'">'+
                                                    '<input type="hidden" name="param" value="kurang">'+
                                                    '<button class="btn btn-primary btn-sm btn-decrease" data-id="'+d.id+'" data-qty="'+d.qty+'" '+disabled+'>-'+
                                                    '</button>'+
                                                    '<button class="btn btn-outline-primary btn-sm" id="qty" disabled="true">'+d.qty+
                                                    '</button>'+
                                                    '<input type="hidden" name="param" value="tambah">'+
                                                    '<button class="btn btn-primary btn-sm btn-increase" data-id="'+d.id+'" data-qty="'+d.produk.id+'">'+
                                                    '+</button>'+
                                                    '</div>'+
                                                    '<h5 class="cart-item-price">Rp'+numberWithCommas(d.subtotal)+'</h5>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                    })
                    $('#cart-total').html('Rp'+(response.total));
                    $('#cart-count').html((response.count));
                    $('#cart-count-label').html((response.count));
                    $('#cart-items').html(rows);
                        
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
            },
            error: function (data) {
                toastr.error('Produk gagal dihapus', 'Gagal!', {
                    closeButton: true,
                    tapToDismiss: false
                });
            }
        });
    });

    
});
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}