<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    public $table = 't_pengiriman';
    protected $fillable = ['pengiriman'];

}
