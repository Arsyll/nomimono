<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rasa extends Model
{
    public $table = 't_rasa';
    protected $fillable = ['rasa'];

        public function produk(){
            return $this->hasOne(Produk::class);
        }
}
