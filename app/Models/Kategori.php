<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    public $table = 't_kategori';
    protected $fillable = ['kategori'];

        public function produk(){
            return $this->hasOne(Produk::class);
        }
}
