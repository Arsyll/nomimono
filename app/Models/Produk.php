<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model{
    public $table = 't_produk';
    protected $fillable =[
        'nama',
        'deskripsi',
        'id_kategori',
        'id_rasa',
        'foto',
        'stok',
        'hpp',
        'harga'
    ];
    public function kategori(){
        return $this->belongsTo(Kategori::class, 'id_kategori');
    }
    public function rasa(){
        return $this->belongsTo(Rasa::class, 'id_rasa');
    }
    public function detail() {
        return $this->hasMany(CartDetail::class, 'cart_id');
    }

}