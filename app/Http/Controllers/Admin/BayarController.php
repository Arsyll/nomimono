<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bayar;
use Illuminate\Support\Facades\DB;
use Auth;

class BayarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['carabayar'] = Bayar::orderBy('cara_bayar','asc')->get();
        if(Auth::user()->role == 'admin'){
            return view('admin.carabayar.index', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return view('user.carabayar.index', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['carabayar'] = Bayar::orderBy('cara_bayar','asc')->get();
        if(Auth::user()->role == 'admin'){
            return view('admin.carabayar.form');
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/carabayar');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'cara_bayar' => 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $status = Bayar::create($input);
        if ($status){
            return redirect('/carabayar')->with('success','Data berhasil ditambahkan');
        }else{
            return redirect('/carabayar/create')->with('error','Data gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['carabayar'] = DB::table('t_cara_bayar')->find($id);
        if(Auth::user()->role == 'admin'){
            return view('admin.carabayar.form', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/carabayar');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['carabayar'] = DB::table('t_cara_bayar')->find($id);
        if(Auth::user()->role == 'admin'){
            return view('admin.carabayar.form', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/carabayar');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'cara_bayar' => 'required'
        ];
        $this->validate($request, $rule);
    
        $input = $request->all();
        // unset($input['_token']);
        // unset($input['_method']);
        // $status = DB::table('t_produk')->where('id', $id)->update($input);

        $carabayar = Bayar::find($id);
        $status = $carabayar->update($input);
        
        // $tproduk = DB::find($id);
        // if ($request->foto){
        //     $fotoName = time() . '.' . $request->foto->extension();
        //     $request->foto->move(public_path() . '/image/foto/', $fotoName);
        // } else {
        //     $fotoName = $tproduk->foto;
        // }

        if ($status){
            return redirect('/carabayar')->with('success','Data berhasil diubah');
        }else{
            return redirect('/carabayar/create')->with('error','Data gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carabayar = Bayar::find($id);
        $status = $carabayar->delete();
        if ($status){
            return redirect('/carabayar')->with('success','Data berhasil dihapus');
        }else{
            return redirect('/carabayar/create')->with('error','Data gagal dihapus');
        }
    }
}
