<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Produk;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Kategori;
use App\Models\Rasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;

class ProdukController extends Controller
{
    public function index(Request $request){
        $itemuser = $request->user();//ambil data user
        $itemcart = Cart::where('user_id', $itemuser->id)
                        ->where('status_cart', 'cart')
                        ->first();
        $kategoris = Kategori::all();
        // $cartdetail = CartDetail::where('cart_id', $itemcart->id)->get();
        $data = array('title' => 'Shopping Cart',
                    'itemcart' => $itemcart,
                    // 'cartdetail' => $cartdetail
                    'kategoris' => $kategoris
                    );
        $produks = Produk::orderBy('nama','asc')->get();
        // $data['produk']=DB::table('t_produk')->orderBy('nama')->get();
        // $data['produk'] = Produk::orderBy('nama','asc')->get();

        if(Auth::user()->role == 'admin'){
            return view('admin.produk.index', $data, compact('produks'));
        }
        elseif(Auth::user()->role == 'user'){            
            $cartdetail = CartDetail::where('cart_id', @$itemcart->id)->get();
            $total = $cartdetail->sum('subtotal');
            return view('user.produk.index', $data, compact('cartdetail','produks','total'));
        }
        
    }

    public function kategori($id){
        return response ()->json([
            'message' => 'List Kategori',
            'data' => Produk::where('kategori_id', $id)->get()
        ]);
    }

    public function create(){
        $kategorilist = Kategori::pluck('kategori','id');
        $kategorilist = ['-' => '-Pilih Kategori-'] + collect($kategorilist)->toArray();
        $rasalist = Rasa::pluck('rasa','id');
        $rasalist = ['-' => '-Pilih Rasa-'] + collect($rasalist)->toArray();
        if(Auth::user()->role == 'admin'){
            return view('admin.produk.form', compact('kategorilist','rasalist'));
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/produk');
        }
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|string',
            'deskripsi' => 'required|string',
            'stok' => 'required|numeric',
            'hpp' => 'required|numeric',
            'harga' => 'required|numeric',
            'foto' => 'required',
            'foto.*' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $imageName = time() . '.' . $request->foto->extension();
        $request->foto->move(public_path() . '/image/foto/', $imageName);
        $produk = Produk::create([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi'],
            'id_kategori' => $request['id_kategori'],
            'id_rasa' => $request['id_rasa'],
            'stok' => $request['stok'],
            'hpp' => $request['hpp'],
            'harga' => $request['harga'],
            'foto' => $imageName
        ]);
        // unset($input['_token']);
        // $status = DB::table('t_produk')->insert($input);

        if ($produk){
            return redirect('/produk')->with('success','Data berhasil ditambahkan');
        }else{
            return redirect('/produk/create')->with('error','Data gagal ditambahkan');
        }
    }
    public function show($id){
        $produk = Produk::find($id);
        return view('admin.produk.detail', compact('produk'));
    }
    public function edit(Request $request, $id){
        $produk = Produk::find($id);
        $kategorilist = Kategori::pluck('kategori','id');
        $kategorilist = ['0' => '-Pilih Kategori-'] + collect($kategorilist)->toArray();
        $rasalist = Rasa::pluck('rasa','id');
        $rasalist = ['0' => '-Pilih Rasa-'] + collect($rasalist)->toArray();
        if(Auth::user()->role == 'admin'){
            return view('admin.produk.form', compact('kategorilist','rasalist','produk'));
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/produk');
        }
    }
    public function update( Request $request, $id){
        $request->validate([
            'nama' => 'required|string',
            'deskripsi' => 'required|string',
            'id_kategori' => 'required',
            'stok' => 'required|numeric',
            'hpp' => 'required|numeric',
            'harga' => 'required|numeric',
            'foto.*' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $produk = Produk::find($id);

        if ($request->foto){
            $imagename = time() . '.' . $request->foto->extension();
            $request->foto->move(public_path() . '/image/foto/', $imagename);
        } else {
            $imagename = $produk->foto;
        }
        $produk->update([
            'nama' => $request['nama'],
            'deskripsi' => $request['deskripsi'],
            'id_kategori' => $request['id_kategori'],
            'id_rasa' => $request['id_rasa'],
            'stok' => $request['stok'],
            'hpp' => $request['hpp'],
            'harga' => $request['harga'],
            'foto' => $imagename
        ]);

        if ($produk){
            return redirect('/produk')->with('success','Data berhasil diubah');
        }else{
            return redirect('/produk/create')->with('error','Data gagal diubah');
        }
    }
    public function destroy($id){
        // $status = DB::table('t_produk')->where('id',$id)->delete();
        $produk = Produk::find($id);
        $status = $produk->delete();
        if ($status){
            return redirect('/produk')->with('success','Data berhasil dihapus');
        }else{
            return redirect('/produk/create')->with('error','Data gagal dihapus');
        }
    }
}

