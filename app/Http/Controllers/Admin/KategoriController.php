<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kategori;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class KategoriController extends Controller
{
    public function index(){
        $data['kategori'] = Kategori::orderBy('kategori','asc')->get();
        return view('admin.kategori.index', $data);
        // $data['produk']=DB::table('t_produk')->orderBy('nama')->get();
        // $data['produk'] = Produk::orderBy('nama','asc')->get();
        
    }
    public function isi(){
        return response()->json([
            'massage' => 'List Kategori',
            'data' => Kategori::all()
        ]);
    }

    public function store(Request $request){
        $rule = [
            'kategori' => 'required|unique:t_kategori',
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $status = Kategori::create($input);

        return response()->json([
            'code' => 200,
            'message' => 'Kategori berhasi ditambah!',
            'data' => $status
        ]);
    }
    
    public function show($id)
    {
        return response()->json([
            'message' => 'detail kategori!',
            'data' => Kategori::findOrFail($id)
        ]);
    }

    public function update(Request $request, $id){
        $rule = [
            'kategori' => 'required'
        ];
        $this->validate($request, $rule);
    
        $input = $request->all();
        // unset($input['_token']);
        // unset($input['_method']);
        // $status = DB::table('t_produk')->where('id', $id)->update($input);

        $kategori = Kategori::find($id);
        $status = $kategori->update($input);

        return response()->json([
            'code' => 200,
            'message' => 'Kategori berhasil diubah!',
            'data' => $status
        ]);
    }
    public function destroy(Kategori $kategori){
        $kategori->findOrFail($kategori->id)->delete($kategori);
        return response()->json([
            'message' => 'Kategori berhasil dihapus!'
        ]);
    }
}
