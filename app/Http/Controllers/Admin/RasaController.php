<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rasa;
use Illuminate\Support\Facades\DB;
use Auth;

class RasaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['rasa'] = Rasa::orderBy('rasa','asc')->get();
        if(Auth::user()->role == 'admin'){
            return view('admin.rasa.index', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return view('user.rasa.index', $data);
        }
    }

    public function isi(){
        return response()->json([
            'massage' => 'List Rasa',
            'data' => Rasa::all()
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'rasa' => 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();
        $status = Rasa::create($input);
        return response()->json([
            'code' => 200,
            'message' => 'Kategori berhasi ditambah!',
            'data' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'detail rasa!',
            'data' => Rasa::findOrFail($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'rasa' => 'required'
        ];
        $this->validate($request, $rule);
    
        $input = $request->all();
        $rasa = Rasa::find($id);
        $status = $rasa->update($input);

        return response()->json([
            'code' => 200,
            'message' => 'Kategori berhasil diubah!',
            'data' => $status
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rasa->findOrFail($rasa->id)->delete($rasa);
        return response()->json([
            'message' => 'rasa berhasil dihapus!'
        ]);
    }
}
