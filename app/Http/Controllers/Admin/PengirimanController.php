<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pengiriman;
use Illuminate\Support\Facades\DB;
use Auth;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pengiriman'] = Pengiriman::orderBy('pengiriman','asc')->get();
        if(Auth::user()->role == 'admin'){
            return view('admin.pengiriman.index', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return view('user.pengiriman.index', $data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['pengiriman'] = Pengiriman::orderBy('pengiriman','asc')->get();
        if(Auth::user()->role == 'admin'){
            return view('admin.pengiriman.form');
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/pengiriman');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'pengiriman' => 'required'
        ];
        $this->validate($request, $rule);

        $input = $request->all();

        $status = Pengiriman::create($input);
        if ($status){
            return redirect('/pengiriman')->with('success','Data berhasil ditambahkan');
        }else{
            return redirect('/pengiriman/create')->with('error','Data gagal ditambahkan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['pengiriman'] = DB::table('t_pengiriman')->find($id);
        if(Auth::user()->role == 'admin'){
            return view('admin.pengiriman.form', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/pengiriman');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['pengiriman'] = DB::table('t_pengiriman')->find($id);
        if(Auth::user()->role == 'admin'){
            return view('admin.pengiriman.form', $data);
        }
        elseif(Auth::user()->role == 'user'){
            return redirect('/pengiriman');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = [
            'pengiriman' => 'required'
        ];
        $this->validate($request, $rule);
    
        $input = $request->all();
        // unset($input['_token']);
        // unset($input['_method']);
        // $status = DB::table('t_produk')->where('id', $id)->update($input);

        $pengiriman = Pengiriman::find($id);
        $status = $pengiriman->update($input);
        
        // $tproduk = DB::find($id);
        // if ($request->foto){
        //     $fotoName = time() . '.' . $request->foto->extension();
        //     $request->foto->move(public_path() . '/image/foto/', $fotoName);
        // } else {
        //     $fotoName = $tproduk->foto;
        // }

        if ($status){
            return redirect('/pengiriman')->with('success','Data berhasil diubah');
        }else{
            return redirect('/pengiriman/create')->with('error','Data gagal diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengiriman = Pengiriman::find($id);
        $status = $pengiriman->delete();
        if ($status){
            return redirect('/pengiriman')->with('success','Data berhasil dihapus');
        }else{
            return redirect('/pengiriman/create')->with('error','Data gagal dihapus');
        }
    }
}
