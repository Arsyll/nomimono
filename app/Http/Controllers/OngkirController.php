<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Http;
use Psr\Http\Message\RequestInterface;
use App\Models\City;
use App\Models\Produk;
use App\Models\Cart;
use App\Models\CartDetail;

class OngkirController extends Controller
{
    public function getCity($id){
        $cities = City::where('provinced_id','=', $id)->pluck('city_name', 'id');

        return json_encode($cities);
    }

    public function getOngkir(Request $request, $courier){
        // cek data dan buat variable untuk api nanti
        if($request->origin && $request->destination && $request->weight && $request->courier){
            $origin = $request->origin;
            $destination = $request->destination;
            $weight = $request->weight;
            $courier = $request->courier;
        }else{
            $origin = '';
            $destination = '';
            $weight = '';
            $courier = '';
        }
        
        // buat variable data dari api rajaongkir
        $result = Http::asForm()->withHeaders([
            'key' => '80d2fe36aecc0bc092a53a56af70d400'
        ])->post('https://api.rajaongkir.com/starter/cost',[
            'origin' => $origin,
            'destination' => $destination,
            'weight' => $weight,
            'courier' => $courier
        ]);

        // bikin isian tabel cart
        $ongkir = $result['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'];
        $itemuser = $request->user();
        $allCart = Cart::where('user_id', $itemuser->id)->where('status_cart', 'cart')->with(['detail.produk'])->first();
        $total = ($allCart->subtotal) + $ongkir - ($allCart->diskon);
        $allCart->update([
            'ekspedisi'=> $courier,
            'ongkir'=> $ongkir,
            'total' => $total
        ]);
        $response = [
            "status"=>"success",
            "message"=>"Produk ditambahkan ke keranjang",
            "ongkir"=>$allCart->ongkir,
            "total"=>$allCart->total
        ];
        return response()->json($response);
    }
}
