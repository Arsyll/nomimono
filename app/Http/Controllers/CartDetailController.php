<?php

namespace App\Http\Controllers;

use App\Models\Produk;
use App\Models\Cart;
use App\Models\CartDetail;
use Illuminate\Http\Request;

class CartDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return abort('404');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'produk_id' => 'required',
        ]);
        $itemuser = $request->user();
        $itemproduk = Produk::findOrFail($request->produk_id);
        // cek dulu apakah sudah ada shopping cart untuk user yang sedang login
        $cart = Cart::where('user_id', $itemuser->id)
                    ->where('status_cart', 'cart')
                    ->first();
        if ($cart) {
            $itemcart = $cart;
        } else {
            $no_invoice = Cart::where('user_id', $itemuser->id)->count();
            //nyari jumlah cart berdasarkan user yang sedang login untuk dibuat no invoice
            $inputancart['user_id'] = $itemuser->id;
            $inputancart['no_invoice'] = 'INV '.str_pad(($no_invoice + 1),'3', '0', STR_PAD_LEFT);
            $inputancart['status_cart'] = 'cart';
            $inputancart['status_pembayaran'] = 'belum';
            $inputancart['status_pengiriman'] = 'belum';
            $itemcart = Cart::create($inputancart);
        }
        // cek dulu apakah sudah ada produk di shopping cart
        $cekdetail = CartDetail::where('cart_id', $itemcart->id)
                                ->where('produk_id', $itemproduk->id)
                                ->first();
        $qty = 1;// diisi 1, karena kita set ordernya 1
        $harga = $itemproduk->harga;//ambil harga produk
        $diskon = $itemproduk->promo != null ? $itemproduk->promo->diskon_nominal: 0; // diskon diambil kalo produk itu ada promo
        $subtotal = ($qty * $harga) - $diskon;//buat subtotalnya

        if ($cekdetail) {
            // update detail di table cart_detail
            $cekdetail->updatedetail($cekdetail, $qty, $harga, $diskon);
            // update subtotal dan total di table cart
            $cekdetail->cart->updatetotal($cekdetail->cart, $subtotal);
        } else {
            $inputan = $request->all();
            $inputan['cart_id'] = $itemcart->id;
            $inputan['produk_id'] = $itemproduk->id;
            $inputan['qty'] = $qty;
            $inputan['harga'] = $harga;
            $inputan['diskon'] = $diskon;
            $inputan['subtotal'] = ($harga * $qty) - $diskon;
            $itemdetail = CartDetail::create($inputan);
            // update subtotal dan total di table cart
            $itemdetail->cart->updatetotal($itemdetail->cart, $subtotal);
        }

        $allCart = $cart->with(['detail.produk'])->first();//mengambil data cart dan isinya
        $cartdetail = $allCart->detail;//mengambil data detail dari cart
        $response = [
            "status"=>"success",
            "message"=>"Produk ditambahkan ke keranjang",
            "total"=>$cartdetail->sum('subtotal'),//membuat total harga dari subtotal tiap produk
            "count"=>$cartdetail->count(),//menghitung ada berapa produk didalam cart
            "data"=>$allCart
        ];
        return response()->json($response);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CartDetail $cartdetail)
    {
        $validatedData = $request->validate([
            'qty' => 'required',
        ]);
        $subtotal = $cartdetail->harga * $validatedData['qty'];
        $cartdetail->update($validatedData);
        $cartdetail->update(['subtotal' => $subtotal]);

        $itemuser = $request->user();
        $cart = Cart::where('user_id', $itemuser->id)->where('status_cart', 'cart')->first();
        $cart->update([
            'subtotal' => $cartdetail->where('cart_id', $cart->id)->sum('subtotal'),
            'total' => $cartdetail->where('cart_id', $cart->id)->sum('subtotal')
        ]);
        $allCart = Cart::where('user_id', $itemuser->id)->where('status_cart', 'cart')->with(['detail.produk'])->first();
        $cd = $allCart->detail;
        $response = [
            "status"=>"success",
            "message"=>"Qty diubah",
            "total"=>$cd->sum('subtotal'),
            "count"=>$cd->count(),
            "data"=>$allCart
        ];
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CartDetail  $cartDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $itemdetail = CartDetail::findOrFail($id);
        // update total cart dulu
        $itemdetail->cart->updatetotal($itemdetail->cart, '-'.$itemdetail->subtotal);
        $itemdetail->delete();

        $itemuser = $request->user();
        $allCart = Cart::where('user_id', $itemuser->id)->where('status_cart', 'cart')->with(['detail.produk'])->first();
        $cartdetail = $allCart->detail;
        $response = [
            "status"=>"success",
            "message"=>"Produk dihapus",
            "total"=>$cartdetail->sum('subtotal'),
            "count"=>$cartdetail->count(),
            "data"=>$allCart
        ];
        return response()->json($response);
    }
}
